import { IUser } from "@/constants/interface";

interface IProps {
  data: IUser[];
}

export default function TestServerSide({ data }: IProps) {
  return (
    <div>
      <p>Test Server Side</p>
      <ul>
        {data?.map((item: IUser) => (
          <li key={item?.id}>{item?.name}</li>
        ))}
      </ul>
    </div>
  );
}

export async function getServerSideProps() {
  const res = await fetch("https://64133c86c469cff60d5d3c26.mockapi.io/users");
  const data = await res.json();

  return {
    props: { data },
  };
}
