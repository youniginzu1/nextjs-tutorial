import { IUser } from "@/constants/interface";

interface IProps {
  data: IUser;
}

export default function TestStatic({ data }: IProps) {
  return (
    <div>
      <h2>Test Static</h2>
      <p>{data?.name}</p>
    </div>
  );
}

export async function getStaticProps({ params }: any) {
  const res = await fetch(
    `https://64133c86c469cff60d5d3c26.mockapi.io/users/${params?.id}`
  );
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
}

export async function getStaticPaths() {
  const res = await fetch("https://64133c86c469cff60d5d3c26.mockapi.io/users");
  const data = await res.json();
  const paths = data?.map((item: any) => ({
    params: {
      id: item?.id,
    },
  }));

  return {
    paths: paths,
    fallback: false,
  };
}
